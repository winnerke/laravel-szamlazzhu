<?php


namespace Winnerke\SzamlazzHu\Contracts;

/**
 * Interface ArrayableInvoiceItemCollection
 * @package Winnerke\SzamlazzHu\Contracts
 */
interface ArrayableItemCollection
{

    /**
     * @see ArrayableItem
     * @return ArrayableItem[]
     */
    function toItemCollectionArray();

}