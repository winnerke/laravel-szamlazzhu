<?php


namespace Winnerke\SzamlazzHu\Client\ApiErrors;


use Exception;

/**
 * Class AuthorizationException
 * @package Winnerke\SzamlazzHu\Client\ApiErrors
 *
 * Abstract authorization exception
 */
abstract class AuthorizationException extends Exception
{

}