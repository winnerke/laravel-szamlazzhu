<?php


namespace Winnerke\SzamlazzHu\Tests\Client\Invoice;


use Winnerke\SzamlazzHu\Client\Client;
use Winnerke\SzamlazzHu\Invoice;

class TestCase extends \Winnerke\SzamlazzHu\Tests\Client\TestCase {

    /**
     * @param null $number
     * @param Client|null $client
     * @return Invoice
     */
    protected function getEmptyInvoice($number = null, Client $client = null)
    {
        $invoice = new Invoice();

        if ($client) {
            $invoice->setClient($client);
        }

        if ($number) {
            $invoice->invoiceNumber = $number;
        }

        return $invoice;
    }

}